export const ADD_NAME = "form/add value name";
export const ADD_SURNAME = "form/add value surname";
export const ADD_AGE = "form/add value age";
export const ADD_ADDRESS = "form/add value address";
export const ADD_PHONE = "form/add value phone";
export const CLEARER_FORM = "form/clearer form";

export const saveName = (data) => ({
  type: ADD_NAME,
  payload: { data },
});
export const saveSurname = (data) => ({
  type: ADD_NAME,
  payload: { data },
});
export const saveAge = (data) => ({
  type: ADD_NAME,
  payload: { data },
});
export const saveAddress = (data) => ({
  type: ADD_NAME,
  payload: { data },
});
export const savePhone = (data) => ({
  type: ADD_NAME,
  payload: { data },
});
export const clearerForm = (data) => ({
  type: CLEARER_FORM,
  payload: { data },
});
