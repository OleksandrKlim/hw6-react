import {
  MODAL_FIRST_INVISIBLE_ACTION_TYPE,
  MODAL_FIRST_VISIBLE_ACTION_TYPE,
  MODAL_SECOND_INVISIBLE_ACTION_TYPE,
  MODAL_SECOND_VISIBLE_ACTION_TYPE,
  MODAL_THERE_INVISIBLE_ACTION_TYPE,
  MODAL_THERE_VISIBLE_ACTION_TYPE,
} from "../actions";

const defaultState = {
  modalVisibleFirst: false,
  modalVisibleSecond: false,
  modalVisibleThere: false,
};

export function modalReducer(state = defaultState, action) {
  switch (action.type) {
    case MODAL_FIRST_INVISIBLE_ACTION_TYPE:
      return { ...state, modalVisibleFirst: false };
    case MODAL_FIRST_VISIBLE_ACTION_TYPE:
      return { ...state, modalVisibleFirst: true };
    case MODAL_SECOND_INVISIBLE_ACTION_TYPE:
      return { ...state, modalVisibleSecond: false };
    case MODAL_SECOND_VISIBLE_ACTION_TYPE:
      return { ...state, modalVisibleSecond: true };
    case MODAL_THERE_INVISIBLE_ACTION_TYPE:
      return { ...state, modalVisibleThere: false };
    case MODAL_THERE_VISIBLE_ACTION_TYPE:
      return { ...state, modalVisibleThere: true };

    default:
      return state;
  }
}
