import { CLEAR_STATE_ARTICLE, GET_STATE_ARTICLE } from "../actions";
import { idReducer } from "./productIdReducer";
describe("add or remove id ", () => {
  const storage = [];
  const ADD_ID = {
    type: GET_STATE_ARTICLE,
    payload: 33,
  };
  test("add id to storage", () => {
    expect(idReducer(storage, ADD_ID)).toEqual([33]);
  });
  const REMOVE_ID = {
    type: CLEAR_STATE_ARTICLE,
  };
  test("remove id from storage", () => {
    expect(idReducer(storage, REMOVE_ID)).toEqual([]);
  });
});
