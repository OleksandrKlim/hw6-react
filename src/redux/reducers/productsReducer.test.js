import { GET_CARD_ERROR, GET_CARD_REQUEST, GET_CARD_SUCCESS } from "../actions";
import { cardReducer } from "./productsReducer";
describe("get product from server", () => {
  const storage = {
    products: [],
    isLoading: false,
  };
  const GETTING_CARD_FROM_SERVER = {
    type: GET_CARD_REQUEST,
  };
  test("waiting for server response ", () => {
    expect(cardReducer(storage, GETTING_CARD_FROM_SERVER)).toEqual({
      products: [],
      isLoading: true,
    });
  });
  const storageDefault = {
    products: [],
    isLoading: true,
  };
  const data = [
    {
      id: 2,
      name: "Jon",
    },
    {
      id: 3,
      name: "Alex",
    },
  ];
  const GET_DATA = {
    type: GET_CARD_SUCCESS,
    payload: { products: data },
  };
  test("request success", () => {
    expect(cardReducer(storageDefault, GET_DATA)).toEqual({
      products: [
        {
          id: 2,
          name: "Jon",
        },
        {
          id: 3,
          name: "Alex",
        },
      ],
      isLoading: false,
    });
  });

  const storageNyu = {
    products: [],
    isLoading: true,
  };
  const ERROR_DATA = {
    type: GET_CARD_ERROR,
  };
  test("if sousing wrong", () => {
    expect(cardReducer(storageNyu, ERROR_DATA)).toEqual({
      products: [],
      isLoading: false,
    });
  });
});
