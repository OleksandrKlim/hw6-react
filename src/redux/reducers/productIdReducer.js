import { GET_STATE_ARTICLE, CLEAR_STATE_ARTICLE } from "../actions";
const article = [];
export function idReducer(state = article, action) {
  switch (action.type) {
    case GET_STATE_ARTICLE:
      return [...state, action.payload];
    case CLEAR_STATE_ARTICLE:
      return [];
    default:
      return state;
  }
}
