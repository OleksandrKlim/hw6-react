import { basketReducer } from "./basketReducer";
import {
  ADD_TO_BASKET_ACTION_TYPE,
  ADD_TO_BASKET_VALUE_ACTION_TYPE,
  CLEARER_BASKET,
  GET_BASKET_LOCAL_STORAGE,
  REMOVE_FROM_BASKET_ACTION_TYPE,
  REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
  SAVE_BASKET_TO_LOCAL_STORAGE,
} from "../actions";
describe("basket reducer", () => {
  const state = { 2: 1, 3: 2 };
  const ADD_TO_BASKET = {
    type: ADD_TO_BASKET_ACTION_TYPE,
    payload: 1,
  };
  test("if i add product is missing in busket ", () => {
    expect(basketReducer(state, ADD_TO_BASKET)).toEqual({ 2: 1, 3: 2, 1: 1 });
  });
  const ADD_TO_BASKET_VALUE = {
    type: ADD_TO_BASKET_VALUE_ACTION_TYPE,
    payload: 3,
  };
  test("if i incremeny value product in busket ", () => {
    expect(basketReducer(state, ADD_TO_BASKET_VALUE)).toEqual({
      1: 1,
      2: 1,
      3: 3,
    });
  });
  const DEL_TO_BASKET_VALUE = {
    type: REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
    payload: 3,
  };
  test("if i decriment value product in busket ", () => {
    expect(basketReducer(state, DEL_TO_BASKET_VALUE)).toEqual({
      1: 1,
      2: 1,
      3: 1,
    });
  });
  const DEL_FROM_BASKET = {
    type: REMOVE_FROM_BASKET_ACTION_TYPE,
    payload: 3,
  };
  test("dell  product from busket ", () => {
    expect(basketReducer(state, DEL_FROM_BASKET)).toEqual({
      1: 1,
      2: 1,
    });
  });
  const basketState = { 1: 3, 2: 3, 4: 1 };
  const SET_BASKET_TO_LOCAL_STORAGE = {
    type: SAVE_BASKET_TO_LOCAL_STORAGE,
  };
  test("set product basket to local storage ", () => {
    expect(basketReducer(basketState, SET_BASKET_TO_LOCAL_STORAGE)).toEqual({
      1: 3,
      2: 3,
      4: 1,
    });
  });
  const localStorage = { 1: 1, 2: 1, 4: 1 };
  const GET_BASKET_FROM_LOCAL_STORAGE = {
    type: GET_BASKET_LOCAL_STORAGE,
    payload: localStorage,
  };
  test("get product basket from local storage ", () => {
    expect(basketReducer({}, GET_BASKET_FROM_LOCAL_STORAGE)).toEqual({
      1: 1,
      2: 1,
      4: 1,
    });
  });
  const CLEARED_ALL_BASKET = {
    type: CLEARER_BASKET,
    payload: localStorage,
  };
  test("Emptying the contents of the basket ", () => {
    expect(basketReducer(basketState, CLEARED_ALL_BASKET)).toEqual({});
  });
});
