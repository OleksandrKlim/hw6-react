import {
  MODAL_FIRST_INVISIBLE_ACTION_TYPE,
  MODAL_FIRST_VISIBLE_ACTION_TYPE,
  MODAL_SECOND_INVISIBLE_ACTION_TYPE,
  MODAL_SECOND_VISIBLE_ACTION_TYPE,
  MODAL_THERE_INVISIBLE_ACTION_TYPE,
  MODAL_THERE_VISIBLE_ACTION_TYPE,
} from "../actions";
import { modalReducer } from "./modalReducer";
describe("All modal visibility", () => {
  const storage = {
    modalVisibleFirst: false,
    modalVisibleSecond: false,
    modalVisibleThere: false,
  };
  const OPEN_FIRST_MODAL = {
    type: MODAL_FIRST_VISIBLE_ACTION_TYPE,
  };
  test("open first modal window", () => {
    expect(modalReducer(storage, OPEN_FIRST_MODAL)).toEqual({
      modalVisibleFirst: true,
      modalVisibleSecond: false,
      modalVisibleThere: false,
    });
  });
  const CLOTH_FIRST_MODAL = {
    type: MODAL_FIRST_INVISIBLE_ACTION_TYPE,
  };
  test("cloth first modal window", () => {
    expect(modalReducer(storage, CLOTH_FIRST_MODAL)).toEqual({
      modalVisibleFirst: false,
      modalVisibleSecond: false,
      modalVisibleThere: false,
    });
  });

  const OPEN_SECOND_MODAL = {
    type: MODAL_SECOND_VISIBLE_ACTION_TYPE,
  };
  test("open second modal window", () => {
    expect(modalReducer(storage, OPEN_SECOND_MODAL)).toEqual({
      modalVisibleFirst: false,
      modalVisibleSecond: true,
      modalVisibleThere: false,
    });
  });
  const CLOTH_SECOND_MODAL = {
    type: MODAL_SECOND_INVISIBLE_ACTION_TYPE,
  };
  test("cloth second modal window", () => {
    expect(modalReducer(storage, CLOTH_SECOND_MODAL)).toEqual({
      modalVisibleFirst: false,
      modalVisibleSecond: false,
      modalVisibleThere: false,
    });
  });
  const CLOTH_MODAL_THREE = {
    type: MODAL_THERE_VISIBLE_ACTION_TYPE,
  };
  test("cloth three modal window", () => {
    expect(modalReducer(storage, CLOTH_MODAL_THREE)).toEqual({
      modalVisibleFirst: false,
      modalVisibleSecond: false,
      modalVisibleThere: true,
    });
  });
  const OPEN_MODAL_THREE = {
    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
  };
  test("open three modal window", () => {
    expect(modalReducer(storage, OPEN_MODAL_THREE)).toEqual({
      modalVisibleFirst: false,
      modalVisibleSecond: false,
      modalVisibleThere: false,
    });
  });
});
