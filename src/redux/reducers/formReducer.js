import { CLOTHE_FORM_SECOND, OPEN_FORM_SECOND } from "../actions";

const defaultState = {
  formSecond: false,
};

export function formReducer(state = defaultState, action) {
  switch (action.type) {
    case OPEN_FORM_SECOND:
      return { ...state, formSecond: true };
    case CLOTHE_FORM_SECOND:
      return { ...state, formSecond: false };

    default:
      return state;
  }
}
