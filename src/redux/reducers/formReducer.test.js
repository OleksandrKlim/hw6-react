import { CLOTHE_FORM_SECOND, OPEN_FORM_SECOND } from "../actions";
import { formReducer } from "./formReducer";
describe("form reducer", () => {
  const state = {
    formSecond: false,
  };
  const OPEN_FORM = {
    type: OPEN_FORM_SECOND,
  };
  test("open form", () => {
    expect(formReducer(state, OPEN_FORM)).toEqual({ formSecond: true });
  });
  const CLOTHE_FORM = {
    type: CLOTHE_FORM_SECOND,
  };
  test("clothe form", () => {
    expect(formReducer(state, CLOTHE_FORM)).toEqual({ formSecond: false });
  });
});
