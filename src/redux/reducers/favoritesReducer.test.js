import {
  ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE,
  DEL_PRODUCT_FROM_FAVORITES_ACTION_TYPE,
  GET_FAVORITES_LOCAL_STORAGE,
  SAVE_BASKET_TO_LOCAL_STORAGE,
} from "../actions";
import { favoritesReducer } from "./favoritesReducer";

describe("favorite reducer", () => {
  const state = [4, 6];
  const ADD_TO_FAVORITES = {
    type: ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE,
    payload: { productId: 3 },
  };
  test("if product is not favorites", () => {
    expect(favoritesReducer(state, ADD_TO_FAVORITES)).toEqual([4, 6, 3]);
  });
  const REMOVE_FAVORITES = {
    type: DEL_PRODUCT_FROM_FAVORITES_ACTION_TYPE,
    payload: { productId: 3 },
  };
  test("if product in favorites remove", () => {
    expect(favoritesReducer(state, REMOVE_FAVORITES)).toEqual([4, 6]);
  });
  const SAVE_FAVORITES_LOCAL_STORAGE = {
    type: SAVE_BASKET_TO_LOCAL_STORAGE,
  };
  test("save favorites to local storage", () => {
    expect(favoritesReducer(state, SAVE_FAVORITES_LOCAL_STORAGE)).toEqual([
      4, 6,
    ]);
  });
  const localStorage = [4, 6];
  const GET_FAVORITES_FROM_LOCAL_STORAGE = {
    type: GET_FAVORITES_LOCAL_STORAGE,
    payload: localStorage,
  };

  test("get favorites from local storage", () => {
    expect(favoritesReducer([], GET_FAVORITES_FROM_LOCAL_STORAGE)).toEqual([
      4, 6,
    ]);
  });
});
