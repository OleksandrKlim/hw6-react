import { createStore, applyMiddleware, compose } from "redux";
import { rootReducer } from "./reducers/rootReducer";
import thunk from "redux-thunk";
import {
  getBasketLocalStorage,
  getLikeLocalStorage,
  saveBasketToLocalStorage,
  saveLikeToLocalStorage,
  
} from "./reducers/thunk";
import { basketSelector, favoritesSelector } from "./selectors";
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

store.dispatch(getLikeLocalStorage());
store.dispatch(getBasketLocalStorage());
store.subscribe(() => {
  const state = store.getState(favoritesSelector);
  store.dispatch(saveLikeToLocalStorage(state.favorites));
});
store.subscribe(() => {
  const state = store.getState(basketSelector);
   store.dispatch(saveBasketToLocalStorage(state.basket));
});
