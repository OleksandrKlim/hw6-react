import { createBrowserRouter } from "react-router-dom";
import Main from "./component/pages/Main/Main";
import { CardList } from "./component/CardList/CardList";
import Basket from "./component/pages/Basket/Basket";
import Like from "./component/pages/Like/Like";
export const router = createBrowserRouter([
  {
    path: "/",
    element: <Main />,
    children: [
      {
        path: "/",
        element: <CardList />,
      },
      {
        path: "/basket",
        element: <Basket />,
      },
      {
        path: "/like",
        element: <Like />,
      },
    ],
  },
]);
