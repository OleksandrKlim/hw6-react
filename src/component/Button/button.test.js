import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { Button } from "./button";

test("test button", () => {
  const handleClick = jest.fn();
  const { container } = render(
    <Button
      idbutton="23"
      class="btn"
      handleClick={handleClick}
      text="Click me"
      context="to proceed"
    />
  );

  const button = screen.getByTestId("23");

  fireEvent.click(button);

  expect(handleClick).toHaveBeenCalledTimes(1);
  expect(container).toMatchSnapshot();
});
