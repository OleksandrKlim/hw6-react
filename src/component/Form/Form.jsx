import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { CLEARER_BASKET, CLOTHE_FORM_SECOND } from "../../redux/actions";
import { basketSelector } from "../../redux/selectors";

export default function Form(props) {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);
  const basket = useSelector(basketSelector);
  const basketProducts = Object.entries(basket).map(([productId, quantity]) => {
    const product = products.find((product) => {
      return product.article === productId;
    });

    return product;
  });
  const byBasket = () => {
    console.log("basket", basketProducts);
    dispatch({
      type: CLEARER_BASKET,
    });
    dispatch({
      type: CLOTHE_FORM_SECOND,
    });
  };

  const formik = useFormik({
    initialValues: {
      Name: "",
      Surname: "",
      Age: "",
      Address: "",
      Phone: "",
    },
    validationSchema: Yup.object({
      Name: Yup.string()
        .matches(
          /^[a-zA-Zа-яА-ЯіІїЇєЄґҐ'’`-]+$/,
          "The field must contain only letters"
        )
        .max(15, "Must be 15 characters or less")
        .required("Обов'язкові поля"),
      Surname: Yup.string()
        .matches(
          /^[a-zA-Zа-яА-ЯіІїЇєЄґҐ'’`-]+$/,
          "The field must contain only letters"
        )
        .max(20, "Must be 20 characters or less")
        .required("Обов'язкові поля"),
      Age: Yup.number("Must be number")
        .positive("Must be positive number")
        .max(150, "Must be  less 150")
        .required("Обов'язкові поля"),
      Address: Yup.string("Must be string")
        .max(40, "Must be 40 characters or less")
        .required("Обов'язкові поля"),
      Phone: Yup.string()
        .matches(/^\d{10}$/, "Phone number is not valid")

        .required("Обов'язкові поля"),
    }),
    onSubmit: (values) => {
      console.log("form", JSON.stringify(values, null, 2));
      byBasket();
    },
  });
  return (
    <div className={props.class} onClick={props.clotheHandel}>
      <form
        className={props.classForm}
        onSubmit={formik.handleSubmit}
        onClick={props.wrapperClose}
      >
        <label className={props.classInput}>{props.text}</label>
        <input
          className={props.classInput}
          id="Name"
          name="Name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Name}
          placeholder="Ім'я"
        />
        {formik.touched.Name && formik.errors.Name ? (
          <div>{formik.errors.Name}</div>
        ) : null}
        <input
          className={props.classInput}
          id="Surname"
          name="Surname"
          type="text"
          placeholder="Прізвище"
          onChange={formik.handleChange}
          value={formik.values.Surname}
        />
        {formik.touched.Surname && formik.errors.Surname ? (
          <div>{formik.errors.Surname}</div>
        ) : null}
        <input
          className={props.classInput}
          id="Age"
          name="Age"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.Age}
          placeholder="Вік "
        />
        {formik.touched.Age && formik.errors.Age ? (
          <div>{formik.errors.Age}</div>
        ) : null}
        <input
          className={props.classInput}
          id="Address"
          name="Address"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Address}
          placeholder="Адреса доставки"
        />
        {formik.touched.Address && formik.errors.Address ? (
          <div>{formik.errors.Address}</div>
        ) : null}
        <input
          className={props.classInput}
          id="Phone"
          name="Phone"
          type="tel"
          onChange={formik.handleChange}
          value={formik.values.email}
          placeholder="Мобільний телефон"
        />
        {formik.touched.Phone && formik.errors.Phone ? (
          <div>{formik.errors.Phone}</div>
        ) : null}

        <div className="modal__cloth" onClick={props.clotheHandel} />
        <button type="submit" className={props.buttonStyle}>
          Checkout
        </button>
      </form>
    </div>
  );
}
