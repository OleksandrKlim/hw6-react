import React from "react";
import Form from "../../Form/Form";
import {
  ADD_TO_BASKET_VALUE_ACTION_TYPE,
  REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
  REMOVE_FROM_BASKET_ACTION_TYPE,
  MODAL_THERE_VISIBLE_ACTION_TYPE,
  MODAL_THERE_INVISIBLE_ACTION_TYPE,
  GET_STATE_ARTICLE,
  CLEAR_STATE_ARTICLE,
  CLOTHE_FORM_SECOND,
  delProductFromFavorites,
  addProductToFavorites,
  OPEN_FORM_SECOND,
} from "../../../redux/actions";
import {
  favoritesSelector,
  formVisibilitySecond,
  modalThere,
  selectedId,
} from "../../../redux/selectors";
import { useContext } from "react";
import ThemeContext from "../../../themeContext";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../Button/Button";
import { Card } from "../../Card/Card";
import { Modal } from "../../Modal/Modal";
import { Svg } from "../../Button/Svg";
import "./basket.scss";

function Basket() {
  const products = useSelector((state) => state.products.products);
  const favorites = useSelector(favoritesSelector);
  const basket = useSelector((state) => state.basket);
  const modal = useSelector(modalThere);
  const articleCard = useSelector(selectedId);
  const formSecond = useSelector(formVisibilitySecond);
  const dispatch = useDispatch();

  const addToLike = (productId) => {
    favorites.includes(productId)
      ? dispatch(delProductFromFavorites(productId))
      : dispatch(addProductToFavorites(productId));
  };

  const openModalDeleteProduct = (article) => {
    dispatch({
      type: MODAL_THERE_VISIBLE_ACTION_TYPE,
    });
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: article,
    });
  };

  const buyEverything = (props) => {
    dispatch({
      type: OPEN_FORM_SECOND,
    });
  };
  const deleteProduct = () => {
    dispatch({
      type: REMOVE_FROM_BASKET_ACTION_TYPE,
      payload: articleCard,
    });
    dispatch({ type: CLEAR_STATE_ARTICLE });
  };

  const clothFormSecond = () => {
    dispatch({
      type: CLOTHE_FORM_SECOND,
    });
  };
  const quantity = (productId) => basket[productId];
  function incrementBasket(article) {
    dispatch({ type: ADD_TO_BASKET_VALUE_ACTION_TYPE, payload: article });
  }
  function decrementBasket(article) {
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: article,
    });
    basket[article] === 1
      ? dispatch({
          type: MODAL_THERE_VISIBLE_ACTION_TYPE,
        })
      : dispatch({
          type: REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
          payload: article,
        });
  }
  const { state } = useContext(ThemeContext);
  if (products.length === 0) {
    return null;
  }

  const basketProducts = Object.entries(basket).map(([productId, quantity]) => {
    const product = products.find((product) => {
      return product.article === productId;
    });

    return product;
  });

  return (
    <>
      {Object.keys(basket).length === 0 ? (
        <h2>Корзина порожня. Додайте будьласка товар</h2>
      ) : (
        <ul className={state === "box" ? "basket" : "basket__toString"}>
          {basketProducts.map((el) => (
            <Card
              styleWrapper={state === "box" ? "basket__list" : "basket__string"}
              product={el}
              key={el.article}
              name={el.name}
              img={el.img}
              prise={el.prise}
              color={el.color}
              article={el.article}
              action={
                <>
                  <div className={state === "box" ? "basket__button-box" : "basket__button-list"}>
                    <Button
                      idbutton="3.6"
                      text="+"
                      handleClick={() => incrementBasket(el.article)}
                    />
                    <p className="basket__prise">{quantity(el.article)}</p>
                    <Button
                      idbutton="3.5"
                      text="-"
                      handleClick={() => decrementBasket(el.article)}
                    />
                  </div>
                  <Button
                    idbutton="3.4"
                    class="basket__btn--favorite"
                    text=<Svg
                      color={favorites.includes(el.article) ? "red" : "white"}
                    />
                    handleClick={() => addToLike(el.article)}
                  />

                  <p className=" basket__sum">
                    Вартість: {quantity(el.article) * el.prise}
                  </p>

                  <div
                    onClick={() => {
                      openModalDeleteProduct(el.article);
                    }}
                    className="basket__clothe"
                  />
                </>
              }
            />
          ))}
          <li>
            <button
              idbutton="3.3"
              className=" basket__byAll"
              onClick={buyEverything}
            >
              Купити обране
            </button>
          </li>
        </ul>
      )}
      {modal && (
        <Modal
          closeButton={true}
          header="Видалити товар з кошику ?"
          text="Click ok  to delete or cancel if you change your mind "
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            dispatch({
              type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
            });
          }}
          action={
            <>
              <Button
                idbutton="3.1"
                text="Ok"
                handleClick={() => {
                  deleteProduct();
                  dispatch({
                    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
                  });
                }}
              />
              <Button
                idbutton="3.2"
                text="Cancel"
                handleClick={() => {
                  dispatch({
                    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
                  });
                }}
              />
            </>
          }
        />
      )}

      {formSecond && (
        <Form
          wrapperClose={(e) => e.stopPropagation()}
          text=" Купити весь обраний товар"
          class="basket__wrapper"
          classForm="basket__form"
          classInput="basket__input"
          clotheHandel={clothFormSecond}
          buttonStyle="basket__btn"
        />
      )}
    </>
  );
}

export default Basket;
