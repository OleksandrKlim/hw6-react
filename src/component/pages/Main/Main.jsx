import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import {
  getBasketLocalStorage,
  productsCardThunk,
  getLikeLocalStorage,
} from "../../../redux/reducers/thunk.js";
import { Header } from "../../Header/Header";
import { Outlet } from "react-router-dom";
import Footer from "../../Footer/Footer";
import "./main.scss";
import { basketSelector } from "../../../redux/selectors";

function Main() {
  const basket = useSelector(basketSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(productsCardThunk());
  }, [dispatch]);
  
  useEffect(() => {
    dispatch(getLikeLocalStorage());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getBasketLocalStorage());
  }, [dispatch]);

  function sumSalaries(salaries) {
    let sum = 0;
    for (let salary of Object.values(basket)) {
      sum += salary;
    }

    return sum;
  }

  return (
    <div className="main__wrapper">
      <Header basket={sumSalaries()} />
      <Outlet />
      <Footer />
    </div>
  );
}

export default Main;
