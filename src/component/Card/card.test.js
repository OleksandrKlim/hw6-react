import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { Card } from "./Card";
import { Button } from "../Button/Button";
import { addToBasket } from "../../redux/actions";
import { Svg } from "../Button/Svg";

test("renders correctly", () => {
  const addToLike = jest.fn();
  const addToBuyClick = jest.fn();
  const product = {
    article: "23",
    name: "Катана",
    img: "катана.jpg",
    prise: "235",
    color: "комбинированный",
  };
  const { container } = render(
    <Card
      key={+product.article}
      name={product.name}
      img={product.img}
      prise={product.prise}
      color={product.color}
      article={product.article}
      action={
        <>
          <Button
            idbutton="23"
            text="Add to cart"
            handleClick={() => addToBuyClick(product.article)}
          />

          <Button
            idbutton="24"
            handleClick={() => {
              addToLike(product.article);
            }}
          />
        </>
      }
    />
  );
  const addToCartButton = screen.getByTestId("23");
  const favoriteCartButton = screen.getByTestId("24");
  expect(addToCartButton).toBeDefined();
  expect(favoriteCartButton).toBeDefined();
  expect(container).toMatchSnapshot();
});
