import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { Modal } from "./Modal";
import { Button } from "../Button/Button";

test("renders with the correct header and body text", () => {
  const onClose = jest.fn();
  const { container } = render(
    <Modal
      closeButton={true}
      header="text"
      text="Click ok to add or cancel if you change your mind"
      wrapperClose={(e) => e.stopPropagation()}
      action={
        <>
          <Button idbutton="1.1" text="Ok" handleClick={onClose} />
          <Button idbutton="1.2" text="Cancel" handleClick={onClose} />
        </>
      }
    />
  );
  const headerElement = screen.getByText("text");
  const bodyElement = screen.getByText(
    "Click ok to add or cancel if you change your mind"
  );
  expect(headerElement).toBeDefined();
  expect(bodyElement).toBeDefined();
  const okButton = screen.getByTestId("1.1");
  const cancelButton = screen.getByTestId("1.2");

  fireEvent.click(okButton);
  expect(onClose).toHaveBeenCalled();

  fireEvent.click(cancelButton);
  expect(onClose).toHaveBeenCalledTimes(2);
  expect(container).toMatchSnapshot();
});
