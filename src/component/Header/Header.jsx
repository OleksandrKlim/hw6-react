import React from "react";
import PropTypes from "prop-types";
import ThemeContext from "../../themeContext";
import "./header.scss";
import { NavLink } from "react-router-dom";
import { useContext } from "react";
import { useSelector } from "react-redux";
import { favoritesSelector } from "../../redux/selectors";

export function Header(props) {
  const favorites = useSelector(favoritesSelector);
  const { state, handlePageChange } = useContext(ThemeContext);
  return (
    <header className="header">
      <nav className="header__nav">
        <NavLink
          to="/"
          className={({ isActive }) => (isActive ? "active-link" : "link")}
        >
          Main
        </NavLink>

        <NavLink
          to="/like"
          className={({ isActive }) => (isActive ? "active-link" : "link")}
        >
          Like
        </NavLink>
        <NavLink
          to="/basket"
          className={({ isActive }) => (isActive ? "active-link" : "link")}
        >
          Basket
        </NavLink>
      </nav>
      <div className="header__wrapper">
        <span className="header__button">Like: {favorites.length}</span>
        <span className="header__button--buy">Basket: {props.basket}</span>
      </div>
      <button
        className={state === "box" ? "box" : "list"}
        onClick={handlePageChange}
      >
        <img src="../../../public/img/free-icon-font-apps-3917456.png" alt="" />
      </button>
    </header>
  );
}

Header.propTypes = {
  text: PropTypes.string,
  class: PropTypes.string,
};
