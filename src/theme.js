import React from "react";
import { useState } from "react";
import ThemeContext from "./themeContext";

const ThemeProvider = ({ children }) => {
  const [state, setState] = useState("box");

  const handlePageChange = () => {
    state === "box" ? setState("string") : setState("box");
  };

  return (
    <ThemeContext.Provider value={{ state, handlePageChange }}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;
