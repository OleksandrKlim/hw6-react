import { RouterProvider } from "react-router-dom";
import { router } from "./router";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import ThemeProvider from "./theme";
function App({ props }) {
  return (
    <ThemeProvider>
      <Provider store={store}>
        <RouterProvider router={router} />;
      </Provider>
    </ThemeProvider>
  );
}

export default App;
